<?php

namespace App\Http\Controllers\Thread;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Thread;
use Illuminate\Http\Request;

class ThreadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $threads = Thread::where('title', 'LIKE', "%$keyword%")
                ->orWhere('first_msg', 'LIKE', "%$keyword%")
                ->orWhere('creator', 'LIKE', "%$keyword%")
                ->orWhere('section', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $threads = Thread::latest()->paginate($perPage);
        }

        return view('threads.threads.index', compact('threads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('threads.threads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required|max:30',
			'first_msg' => 'required',
			'creator' => 'required',
			'section' => 'required'
		]);
        $requestData = $request->all();
        
        Thread::create($requestData);

        return redirect('threads/threads')->with('flash_message', 'Thread added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $thread = Thread::findOrFail($id);

        return view('threads.threads.show', compact('thread'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $thread = Thread::findOrFail($id);

        return view('threads.threads.edit', compact('thread'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required|max:30',
			'first_msg' => 'required',
			'creator' => 'required',
			'section' => 'required'
		]);
        $requestData = $request->all();
        
        $thread = Thread::findOrFail($id);
        $thread->update($requestData);

        return redirect('threads/threads')->with('flash_message', 'Thread updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Thread::destroy($id);

        return redirect('threads/threads')->with('flash_message', 'Thread deleted!');
    }
}
