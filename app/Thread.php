<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'threads';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'first_msg', 'creator', 'section'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function section()
    {
        return $this->belongsTo('App\Section');
    }
    
}
