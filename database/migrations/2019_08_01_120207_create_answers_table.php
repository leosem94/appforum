<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('content')->nullable();
            $table->integer('ref_thread')->unsigned();
            $table->integer('sender_user')->unsigned();
            $table->foreign('ref_thread')->references('id')->on('threads')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sender_user')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('answers');
    }
}
