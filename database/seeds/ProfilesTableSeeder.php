<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'user_name' => Str::random(10),
            'country' => Str::random(10),
            'user_id' => rand(10, 22),
        ]);
    }
}
