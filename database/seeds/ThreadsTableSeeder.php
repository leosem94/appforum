<?php

use Illuminate\Database\Seeder;

class ThreadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('threads')->insert([
            'title' => Str::random(10),
            'first_msg' => Str::random(10),
            'creator' => rand(10,15),
            'section' => rand(40,45),
        ]);
    }
}
