<?php

use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('sections')->insert([
            'topic' => Str::random(10),
            //'thread_id' => rand(40,45),
        ]);
    }
}
