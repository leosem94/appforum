<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('admin/admin', 'Admin\\AdminController');
Route::resource('threads/threads', 'Thread\\ThreadsController');
Route::resource('sections/sections', 'Section\\SectionsController');

Route::resource('answers/answers', 'Answer\\AnswersController');




Route::resource('profiles/profiles', 'Profile\\ProfilesController');
Route::resource('profiles/profiles', 'Profile\\ProfilesController');
Route::resource('profiles/profiles', 'Profile\\ProfilesController');
Route::resource('profiles/profiles', 'Profile\\ProfilesController');