<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
    <label for="content" class="control-label">{{ 'Content' }}</label>
    <input class="form-control" name="content" type="text" id="content" value="{{ isset($answer->content) ? $answer->content : ''}}" required>
    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ref_thread') ? 'has-error' : ''}}">
    <label for="ref_thread" class="control-label">{{ 'Ref Thread' }}</label>
    <input class="form-control" name="ref_thread" type="number" id="ref_thread" value="{{ isset($answer->ref_thread) ? $answer->ref_thread : ''}}" required>
    {!! $errors->first('ref_thread', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('sender_user') ? 'has-error' : ''}}">
    <label for="sender_user" class="control-label">{{ 'Sender User' }}</label>
    <input class="form-control" name="sender_user" type="number" id="sender_user" value="{{ isset($answer->sender_user) ? $answer->sender_user : ''}}" required>
    {!! $errors->first('sender_user', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
