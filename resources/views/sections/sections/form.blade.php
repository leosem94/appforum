<div class="form-group {{ $errors->has('topic') ? 'has-error' : ''}}">
    <label for="topic" class="control-label">{{ 'Topic' }}</label>
    <input class="form-control" name="topic" type="text" id="topic" value="{{ isset($section->topic) ? $section->topic : ''}}" required>
    {!! $errors->first('topic', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('thread_id') ? 'has-error' : ''}}">
    <label for="thread_id" class="control-label">{{ 'Thread Id' }}</label>
    <input class="form-control" name="thread_id" type="number" id="thread_id" value="{{ isset($section->thread_id) ? $section->thread_id : ''}}" required>
    {!! $errors->first('thread_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
