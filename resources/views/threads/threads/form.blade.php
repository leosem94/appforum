<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($thread->title) ? $thread->title : ''}}" required>
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('first_msg') ? 'has-error' : ''}}">
    <label for="first_msg" class="control-label">{{ 'First Msg' }}</label>
    <textarea class="form-control" rows="5" name="first_msg" type="textarea" id="first_msg" required>{{ isset($thread->first_msg) ? $thread->first_msg : ''}}</textarea>
    {!! $errors->first('first_msg', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('creator') ? 'has-error' : ''}}">
    <label for="creator" class="control-label">{{ 'Creator' }}</label>
    <input class="form-control" name="creator" type="number" id="creator" value="{{ isset($thread->creator) ? $thread->creator : ''}}" required>
    {!! $errors->first('creator', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('section') ? 'has-error' : ''}}">
    <label for="section" class="control-label">{{ 'Section' }}</label>
    <input class="form-control" name="section" type="number" id="section" value="{{ isset($thread->section) ? $thread->section : ''}}" required>
    {!! $errors->first('section', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
