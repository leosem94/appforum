<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <ul class="nav flex-column" role="tablist">

                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="{{ url('/admin/admin') }}">
                        Dashboard
                    </a>
                </li>

                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="{{ url('/answers/answers') }}">
                        Answers
                    </a>
                </li>

                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="{{ url('/profiles/profiles') }}">
                        Profiles
                    </a>
                </li>

                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="{{ url('/sections/sections') }}">
                        Sections
                    </a>
                </li>

                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="{{ url('/threads/threads') }}">
                        Threads
                    </a>
                </li>

            </ul>
        </div>
    </div>
</div>

